# MA Lightning Macros

### Installing

* install e.g. git-bash
* change to `C:\ProgramData\Ma Lightning Technologies\grandma\vXXYY`
* move macros to macros.orig 
* git clone https://gitlab.com/pro-light/ma-lightning-macros.git macros
* To get your original predefined macros : cp macros.orig/predefined.xml macros/
* there is also a `predefined` folder

## ToDo

- improve gma2
- test gma3

## Contributing

Merge requests welcome

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License


The contents of the predefined folder are property of MA Lightning

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
